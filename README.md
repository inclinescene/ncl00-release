# NCL00 - "Just what you've come to expect from us"

Shadertoy demo for @​party 2016
Placed 2nd in the PC Demo Compo

Useful links:

https://www.khronos.org/files/webgl/webgl-reference-card-1_0.pdf

https://www.shadertoy.com/view/ldfSW2 for 303

Matrices:
http://www.purplemath.com/modules/mtrxinvr.htm
https://www.opengl.org/wiki/Compute_eye_space_from_window_space
	(although I didn't wind up using this much)
https://en.wikibooks.org/wiki/GLSL_Programming/Vertex_Transformations

Raycasting:
http://antongerdelan.net/opengl/raycasting.html
http://bookofhook.com/mousepick.pdf
	(linked to by https://gist.github.com/gyng/8921328)
http://lodev.org/cgtutor/raycasting.html

